var app = angular.module('Blog', ['ngResource','ngRoute']);

app.config(['$routeProvider', function($routeProvider){
    $routeProvider
      .when('/',{
        templateUrl: 'partials/home.html',
        controller: 'HomeCtrl'
      })
      .when('/new-post',{
        templateUrl: 'partials/new-post.html',
        controller: 'NewPostCtrl'
      })
      .when('/post/:id', { /* For the hyperlinking of post titles */
        templateUrl: 'partials/new-post.html',
        controller: 'EditPostCtrl'
      })
      .when('/post/:id/delete', {
        templateUrl: 'partials/delete-post.html',
        controller: 'DeletePostCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
}]);

app.controller('HomeCtrl', ['$scope', '$resource',
  function($scope, $resource){
    var Posts = $resource('/api/posts');
    Posts.query(function(post){
      $scope.post = post;
    });
  }]);

/*
 *  This controller has three dependencies: $scope as the glue between the controller
 *  and the view, $resource for working with our RESTful API, and $location for changing
 *  the URL in the browser address bar.
 */
app.controller('NewPostCtrl', ['$scope','$resource', '$location',
  function($scope, $resource, $location) {
    $scope.save = function(){
      var Posts = $resource('/api/posts');
      Posts.save($scope.post, function(){
        $location.path('/');
      });
  };
}]);

app.controller('EditPostCtrl', ['$scope', '$resource', '$location', '$routeParams',
  function($scope, $resource, $location, $routeParams){
    var Posts = $resource('/api/posts/:id', { id: '@_id' }, {
      update: {method: 'PUT'}
    });

    Posts.get({ id: $routeParams.id}, function(post){
      $scope.post = post;
    });

    $scope.save = function(){
      Posts.update($scope.post, function(){
        $location.path('/');
      });
    }
}]);

app.controller('DeletePostCtrl', ['$scope', '$resource', '$location', '$routeParams',
  function($scope, $resource, $location, $routeParams){
    var Posts = $resource('/api/posts/:id');

    Posts.get({ id: $routeParams.id}, function(post){
      $scope.post = post;
    });

    $scope.delete = function(){
      Posts.delete({ id: $routeParams.id}, function(post){
        $location.path('/');
      });
    }

}]);
