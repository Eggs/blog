var express = require('express');
var router = express.Router();

var monk = require('monk');
var db = monk('localhost:27017/blog_db');

router.get('/', function(req, res){
      var collection = db.get('posts');
      collection.find({}, function(err, posts){
        if (err) throw err;
        res.json(posts);
      });

    });


router.post('/', function(req, res){
  var collection = db.get('posts');
  collection.insert({
    title: req.body.title,
    content: req.body.content
  },
    function(err, post){
        if (err) throw err;
        res.json(video);
    });
});

router.get('/:id', function(req, res){
  var collection = db.get('posts');
  collection.findOne({_id: req.params.id}, function(err, post){
    if (err) throw err;

    res.json(post);
  });

});

router.put('/:id', function(req, res){
  var collection = db.get('posts');

  collection.update({_id: req.params.id}, {
    title: req.body.title,
    content: req.body.content
  }, function(err, post){
    if (err) throw err;
    res.json(post);
  });

});

router.delete('/:id', function(req, res){
  var collection = db.get('posts');

  collection.remove({_id: req.params.id}, function(err, post){
    if (err) throw err;
    res.json(post);
  })
});

module.exports = router;
